#include <Arduino.h>

//pin define

#define RXD 0
#define TXD 1
#define INT0 2
#define INT1 3
#define T0 4
#define T1 5
#define AIN0 6
#define AIN1 7
#define ICP 8
#define OC1 9
#define SS 10
#define MOSI 11
#define MISO 12
#define LEDpin 13
#define A0 14
#define A1 15
#define A2 16
#define A3 17
#define A4 18
#define A5 19
 