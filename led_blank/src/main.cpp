/* *******************
Project : LED Blank
Date : 2019.12.20
********************* */
//Basic header file
#include <Arduino.h>

//User header file
#include "define.h"
#include "led_blank.h"

void setup() {

  //borate setting
  Serial.begin(9600); //시리얼통신 세팅

  //pin description
  pinMode(LEDpin, OUTPUT); // LEDpin(13) 출력핀으로 설정
}

void loop() {

  Blank_LED(1000, 1000); //Blank_LED(LED ON 시간, LED OFF 시간)

}

