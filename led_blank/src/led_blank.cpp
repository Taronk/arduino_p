#include <Arduino.h>
#include "led_blank.h"
#include "define.h"

void Blank_LED(int On_time, int Off_time){

  //LED On
  digitalWrite(LEDpin, HIGH);
  Serial.println("LED ON");
  delay(On_time);

  //LED Off
  digitalWrite(LEDpin, LOW);
  Serial.println("LED OFF");
  delay(Off_time);
  
}